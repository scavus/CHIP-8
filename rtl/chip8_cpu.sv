`include "defines.sv"
import defines::*;

module chip8_cpu
  (input logic     i_clk,
   input logic     i_reset,
   intf_ram.master wram_bus,
   intf_ppu.master ppu_bus);

   localparam TIMER_DIVISOR = 1666666;

   typedef enum {
      INSTR_OP_NOP,
      INSTR_OP_PPUCLR,
      INSTR_OP_RET,
      INSTR_OP_JMP,
      INSTR_OP_CALL,
      INSTR_OP_BRANCH,
      INSTR_OP_BXNENN,
      INSTR_OP_BXEQNN,
      INSTR_OP_BXNEY,
      INSTR_OP_BXEQY,
      INSTR_OP_ALU,
      INSTR_OP_SETI,
      INSTR_OP_JMP0,
      INSTR_OP_RAND,
      INSTR_OP_PPUDRW,
      INSTR_OP_KEYF,
      INSTR_OP_KEYT,
      INSTR_OP_GETDT,
      INSTR_OP_GETKEY,
      INSTR_OP_SETDT,
      INSTR_OP_SETST,
      INSTR_OP_ADDIX,
      INSTR_OP_HEX,
      INSTR_OP_BCD,
      INSTR_OP_SAVE,
      INSTR_OP_LOAD
   } instr_op_t;

   typedef enum {
      ALU_OP_NOP,
      ALU_OP_MOV,
      ALU_OP_AND,
      ALU_OP_OR,
      ALU_OP_XOR,
      ALU_OP_ADD,
      ALU_OP_SUB,
      ALU_OP_SHR,
      ALU_OP_SHL
   } alu_op_t;

   typedef enum {
      F_CARRY,
      F_BORROW,
      F_LSB,
      F_MSB,
      F_COL,
      F_NOP
   } flag_t;

   typedef enum {
      S_TERMINATE,
      S_IFETCH,
      S_IFETCH_READ_HI,
      S_IFETCH_READ_LO,
      S_IDECODE,
      S_EXECUTE,
      S_PPU_WAIT,
      S_BCD_WAIT,
      S_BCD_WRITE,
      S_SAVE,
      S_LOAD
   } state_t;

   typedef struct packed {
      instr_op_t                                  op;
      logic [($clog2(CHIP8_NUM_REGISTERS) - 1):0] x;
      logic [($clog2(CHIP8_NUM_REGISTERS) - 1):0] y;
      logic [11:0]                                nnn;
      logic [7:0]                                 nn;
      logic [3:0]                                 n;
   } instr_t;

   typedef struct packed {
      alu_op_t     op;
      logic [15:0] in0;
      logic [15:0] in1;
      flag_t       flag;
   } alu_t;

   typedef struct packed {
      logic [(CHIP8_DATA_WIDTH - 1):0] data_in;
      logic                            vld;
   } bcd_in_t;

   typedef struct packed {
      logic [(4 * 3 - 1):0] data_out;
      logic                 rdy;
   } bcd_out_t;

   logic [(CHIP8_ADDR_WIDTH - 1):0]            pc;
   logic [(CHIP8_INSTR_WIDTH - 1):0]           ir;
   instr_t                                     instr;
   logic [(CHIP8_DATA_WIDTH - 1):0]            r_v [CHIP8_NUM_REGISTERS];
   logic [($clog2(CHIP8_NUM_REGISTERS) - 1):0] r_v_idx;
   logic [(CHIP8_ADDR_WIDTH - 1):0]            r_i;
   logic [(CHIP8_DATA_WIDTH - 1):0]            r_delay_timer;
   logic [(CHIP8_DATA_WIDTH - 1):0]            r_sound_timer;
   logic [23:0]                                timer_tick_counter;
   logic                                       timer_tick;
   logic [(CHIP8_DATA_WIDTH - 1):0]            r_key;
   logic [(CHIP8_ADDR_WIDTH - 1):0]            stack [CHIP8_STACK_SIZE];
   logic [($clog2(CHIP8_STACK_SIZE) - 1):0]    sp;
   logic [15:0]                                alu_out;
   alu_t                                       alu;
   bcd_in_t                                    bcd_in;
   bcd_out_t                                   bcd_out;
   logic [3:0]                                 bcd_data [3];
   logic [1:0]                                 bcd_idx;
   logic [(CHIP8_DATA_WIDTH - 1):0]            prng_out;
   state_t                                     state;

   always_ff @(posedge i_clk, posedge i_reset)
     begin
        if (i_reset) begin
           wram_bus.m_avalid <= 0;
           wram_bus.m_wvalid <= 0;
           ppu_bus.m_vld <= 0;
           bcd_in.vld <= 0;
           pc <= 16'h0200;
           state <= S_IFETCH;
        end else begin
           wram_bus.m_avalid <= 0;
           wram_bus.m_wvalid <= 0;
           ppu_bus.m_vld <= 0;
           bcd_in.vld <= 0;
           if (timer_tick) begin
              r_delay_timer <= r_delay_timer == 0 ? 0 : r_delay_timer - 1;
              r_sound_timer <= r_sound_timer == 0 ? 0 : r_sound_timer - 1;
           end
           case (state)
             S_IFETCH:
               begin
                  if (wram_bus.s_aready) begin
                     wram_bus.m_aaddr <= pc;
                     wram_bus.m_alen <= 1;
                     wram_bus.m_awr <= 0;
                     wram_bus.m_avalid <= 1;
                     wram_bus.m_rready <= 1;
                     state <= S_IFETCH_READ_HI;
                  end
               end
             S_IFETCH_READ_HI:
               begin
                  if (wram_bus.s_rvalid) begin
                     ir[15:8] <= wram_bus.s_rdata;
                     state <= S_IFETCH_READ_LO;
                  end
               end
             S_IFETCH_READ_LO:
               begin
                  if (wram_bus.s_rvalid) begin
                     ir[7:0] <= wram_bus.s_rdata;
                     state <= S_EXECUTE;
                  end
               end
             S_EXECUTE:
               begin
                  pc <= pc + 2;
                  state <= S_IFETCH;
                  case (alu.flag)
                    F_CARRY:  r_v[4'hF] <= CHIP8_DATA_WIDTH'(alu_out[8]);
                    F_BORROW: r_v[4'hF] <= CHIP8_DATA_WIDTH'(!alu_out[8]);
                    F_LSB:    r_v[4'hF] <= CHIP8_DATA_WIDTH'(alu_out[15]);
                    F_MSB:    r_v[4'hF] <= CHIP8_DATA_WIDTH'(alu_out[8]);
                    default:;
                  endcase
                  case (instr.op)
                    INSTR_OP_PPUCLR:
                      begin
                         ppu_bus.cmd.op <= PPU_OP_CLEAR;
                         ppu_bus.m_vld <= 1;
                         state <= S_PPU_WAIT;
                      end
                    INSTR_OP_RET:
                      begin
                         pc <= stack[sp - 1];
                         sp <= sp - 1;
                      end
                    INSTR_OP_JMP: pc <= CHIP8_ADDR_WIDTH'(instr.nnn);
                    INSTR_OP_CALL:
                      begin
                         pc <= CHIP8_ADDR_WIDTH'(instr.nnn);
                         stack[sp] <= pc + 2;
                         sp <= sp + 1;
                      end
                    INSTR_OP_BXNENN: if (r_v[instr.x] == instr.nn) pc <= alu_out;
                    INSTR_OP_BXEQNN: if (r_v[instr.x] != instr.nn) pc <= alu_out;
                    INSTR_OP_BXNEY:  if (r_v[instr.x] == r_v[instr.y]) pc <= alu_out;
                    INSTR_OP_ALU:    r_v[instr.x] <= alu_out[7:0];
                    INSTR_OP_BXEQY:  if (r_v[instr.x] != r_v[instr.y]) pc <= alu_out;
                    INSTR_OP_SETI:   r_i <= CHIP8_ADDR_WIDTH'(instr.nnn);
                    INSTR_OP_JMP0:   pc <= alu_out;
                    INSTR_OP_RAND:   r_v[instr.x] <= prng_out & instr.nn;
                    INSTR_OP_PPUDRW:
                      begin
                         ppu_bus.cmd.op <= PPU_OP_DRAW_SPRITE;
                         ppu_bus.cmd.x <= r_v[instr.x];
                         ppu_bus.cmd.y <= r_v[instr.y];
                         ppu_bus.cmd.h <= instr.n;
                         ppu_bus.cmd.addr <= r_i;
                         ppu_bus.m_vld <= 1;
                         state <= S_PPU_WAIT;
                      end
                    INSTR_OP_KEYF:  if (r_v[instr.x] == r_key) pc <= alu_out;
                    INSTR_OP_KEYT:  if (r_v[instr.x] != r_key) pc <= alu_out;
                    INSTR_OP_GETDT: r_v[instr.x] <= r_delay_timer;
                    INSTR_OP_GETKEY:
                      begin
                         // TODO:
                         // Wait for a keypress and store the result in register VX
                      end
                    INSTR_OP_SETDT: r_delay_timer <= r_v[instr.x];
                    INSTR_OP_SETST: r_sound_timer <= r_v[instr.x];
                    INSTR_OP_ADDIX: r_i <= alu_out;
                    INSTR_OP_HEX:   r_i <= hex_to_sprite_addr(4'(r_v[instr.x]));
                    INSTR_OP_BCD:
                      begin
                         if (bcd_out.rdy) begin
                            bcd_in.data_in <= r_v[instr.x];
                            bcd_in.vld <= 1;
                            state <= S_BCD_WAIT;
                         end else begin
                            state <= state;
                         end
                      end
                    INSTR_OP_SAVE:
                      begin
                         if (wram_bus.s_aready) begin
                            wram_bus.m_aaddr <= r_i;
                            wram_bus.m_alen <= 8'(instr.x);
                            wram_bus.m_awr <= 1;
                            wram_bus.m_avalid <= 1;
                            r_v_idx <= 0;
                            state <= S_SAVE;
                         end else begin
                            state <= state;
                         end
                      end
                    INSTR_OP_LOAD:
                      begin
                         if (wram_bus.s_aready) begin
                            wram_bus.m_aaddr <= r_i;
                            wram_bus.m_alen <= 8'(instr.x);
                            wram_bus.m_awr <= 0;
                            wram_bus.m_avalid <= 1;
                            wram_bus.m_rready <= 1;
                            r_v_idx <= 0;
                            state <= S_LOAD;
                         end else begin
                            state <= state;
                         end
                      end
                    default: state <= S_TERMINATE;
                  endcase
               end
             S_PPU_WAIT:
               begin
                  ppu_bus.cmd.op <= PPU_OP_NOP;
                  ppu_bus.m_vld <= 0;
                  if (ppu_bus.s_rdy && !ppu_bus.m_vld) begin
                     r_v[4'hF] <= ppu_bus.collision;
                     state <= S_IFETCH;
                  end
               end
             S_BCD_WAIT:
               begin
                  bcd_in.vld <= 0;
                  if (bcd_out.rdy && !bcd_in.vld && wram_bus.s_aready) begin
                     bcd_data[0] <= bcd_out.data_out[11:8];
                     bcd_data[1] <= bcd_out.data_out[7:4];
                     bcd_data[2] <= bcd_out.data_out[3:0];
                     wram_bus.m_aaddr <= r_i;
                     wram_bus.m_alen <= 2;
                     wram_bus.m_awr <= 1;
                     wram_bus.m_avalid <= 1;
                     state <= S_BCD_WRITE;
                  end
               end
             S_BCD_WRITE:
               begin
                  if (wram_bus.s_wready) begin
                     wram_bus.m_wdata <= CHIP8_DATA_WIDTH'(bcd_data[bcd_idx]);
                     wram_bus.m_wvalid <= 1;
                     if (bcd_idx == 2) begin
                        bcd_idx <= 0;
                        state <= S_IFETCH;
                     end else begin
                        bcd_idx <= bcd_idx + 1;
                     end
                  end
               end
             S_SAVE:
               begin
                  if (wram_bus.s_wready) begin
                     wram_bus.m_wdata <= r_v[r_v_idx];
                     wram_bus.m_wvalid <= 1;
                     if (r_v_idx == instr.x) begin
                        r_v_idx <= 0;
                        r_i <= alu_out + 1;
                        state <= S_IFETCH;
                     end else begin
                        r_v_idx <= r_v_idx + 1;
                     end
                  end
               end
             S_LOAD:
               begin
                  if (wram_bus.s_rvalid) begin
                     r_v[r_v_idx] <= wram_bus.s_rdata;
                     wram_bus.m_rready <= 1;
                     if (r_v_idx == instr.x) begin
                        r_v_idx <= 0;
                        r_i <= alu_out + 1;
                        state <= S_IFETCH;
                     end else begin
                        r_v_idx <= r_v_idx + 1;
                     end
                  end
               end
             default:;
           endcase
        end
     end

   always_ff @(posedge i_clk)
     begin
        if (timer_tick_counter == TIMER_DIVISOR) begin
           timer_tick <= 1;
           timer_tick_counter <= 0;
        end else begin
           timer_tick <= 0;
           timer_tick_counter <= timer_tick_counter + 1;
        end
     end

   always_comb
     begin: INSTR_DECODER
        instr.op = INSTR_OP_NOP;
        instr.x = ir[11:8];
        instr.y = ir[7:4];
        instr.nnn = ir[11:0];
        instr.nn = ir[7:0];
        instr.n = ir[3:0];
        casez (ir)
          16'h00E0: {instr.op, alu} = {INSTR_OP_PPUCLR, ALU_OP_NOP, 16'd0,             16'd0,             F_NOP   };
          16'h00EE: {instr.op, alu} = {INSTR_OP_RET,    ALU_OP_NOP, 16'd0,             16'd0,             F_NOP   };
          16'h1???: {instr.op, alu} = {INSTR_OP_JMP,    ALU_OP_NOP, 16'd0,             16'd0,             F_NOP   };
          16'h2???: {instr.op, alu} = {INSTR_OP_CALL,   ALU_OP_NOP, 16'd0,             16'd0,             F_NOP   };
          16'h3???: {instr.op, alu} = {INSTR_OP_BXNENN, ALU_OP_ADD, pc,                16'd4,             F_NOP   };
          16'h4???: {instr.op, alu} = {INSTR_OP_BXEQNN, ALU_OP_ADD, pc,                16'd4,             F_NOP   };
          16'h5??0: {instr.op, alu} = {INSTR_OP_BXNEY,  ALU_OP_ADD, pc,                16'd4,             F_NOP   };
          16'h6???: {instr.op, alu} = {INSTR_OP_ALU,    ALU_OP_MOV, 16'(instr.nn),     16'd0,             F_NOP   };
          16'h7???: {instr.op, alu} = {INSTR_OP_ALU,    ALU_OP_ADD, 16'(r_v[instr.x]), 16'(instr.nn),     F_NOP   };
          16'h8??0: {instr.op, alu} = {INSTR_OP_ALU,    ALU_OP_MOV, 16'(r_v[instr.y]), 16'd0,             F_NOP   };
          16'h8??1: {instr.op, alu} = {INSTR_OP_ALU,    ALU_OP_OR,  16'(r_v[instr.x]), 16'(r_v[instr.y]), F_NOP   };
          16'h8??2: {instr.op, alu} = {INSTR_OP_ALU,    ALU_OP_AND, 16'(r_v[instr.x]), 16'(r_v[instr.y]), F_NOP   };
          16'h8??3: {instr.op, alu} = {INSTR_OP_ALU,    ALU_OP_XOR, 16'(r_v[instr.x]), 16'(r_v[instr.y]), F_NOP   };
          16'h8??4: {instr.op, alu} = {INSTR_OP_ALU,    ALU_OP_ADD, 16'(r_v[instr.x]), 16'(r_v[instr.y]), F_CARRY };
          16'h8??5: {instr.op, alu} = {INSTR_OP_ALU,    ALU_OP_SUB, 16'(r_v[instr.x]), 16'(r_v[instr.y]), F_BORROW};
          16'h8??6: {instr.op, alu} = {INSTR_OP_ALU,    ALU_OP_SHR, 16'(r_v[instr.y]), 16'd0,             F_LSB   };
          16'h8??7: {instr.op, alu} = {INSTR_OP_ALU,    ALU_OP_SUB, 16'(r_v[instr.y]), 16'(r_v[instr.x]), F_BORROW};
          16'h8??E: {instr.op, alu} = {INSTR_OP_ALU,    ALU_OP_SHL, 16'(r_v[instr.y]), 16'd0,             F_MSB   };
          16'h9??0: {instr.op, alu} = {INSTR_OP_BXEQY,  ALU_OP_ADD, pc,                16'd4,             F_NOP   };
          16'hA???: {instr.op, alu} = {INSTR_OP_SETI,   ALU_OP_NOP, 16'd0,             16'd0,             F_NOP   };
          16'hB???: {instr.op, alu} = {INSTR_OP_JMP0,   ALU_OP_ADD, 16'(r_v[4'h0]),    16'(instr.nnn),    F_NOP   };
          16'hC???: {instr.op, alu} = {INSTR_OP_RAND,   ALU_OP_NOP, 16'd0,             16'd0,             F_NOP   };
          16'hD???: {instr.op, alu} = {INSTR_OP_PPUDRW, ALU_OP_NOP, 16'd0,             16'd0,             F_COL   };
          16'hE?9E: {instr.op, alu} = {INSTR_OP_KEYF,   ALU_OP_ADD, pc,                16'd4,             F_NOP   };
          16'hE?A1: {instr.op, alu} = {INSTR_OP_KEYT,   ALU_OP_ADD, pc,                16'd4,             F_NOP   };
          16'hF?07: {instr.op, alu} = {INSTR_OP_GETDT,  ALU_OP_NOP, 16'd0,             16'd0,             F_NOP   };
          16'hF?0A: {instr.op, alu} = {INSTR_OP_GETKEY, ALU_OP_NOP, 16'd0,             16'd0,             F_NOP   };
          16'hF?15: {instr.op, alu} = {INSTR_OP_SETDT,  ALU_OP_NOP, 16'd0,             16'd0,             F_NOP   };
          16'hF?18: {instr.op, alu} = {INSTR_OP_SETST,  ALU_OP_NOP, 16'd0,             16'd0,             F_NOP   };
          16'hF?1E: {instr.op, alu} = {INSTR_OP_ADDIX,  ALU_OP_ADD, r_i,               16'(r_v[instr.x]), F_NOP   };
          16'hF?29: {instr.op, alu} = {INSTR_OP_HEX,    ALU_OP_NOP, 16'd0,             16'd0,             F_NOP   };
          16'hF?33: {instr.op, alu} = {INSTR_OP_BCD,    ALU_OP_NOP, 16'd0,             16'd0,             F_NOP   };
          16'hF?55: {instr.op, alu} = {INSTR_OP_SAVE,   ALU_OP_ADD, r_i,               16'(instr.x),      F_NOP   };
          16'hF?65: {instr.op, alu} = {INSTR_OP_LOAD,   ALU_OP_ADD, r_i,               16'(instr.x),      F_NOP   };
          default:  {instr.op, alu} = {INSTR_OP_NOP,    ALU_OP_NOP, 16'd0,             16'd0,             F_NOP   };
        endcase
     end

   always_comb
     begin: ALU
        unique case (alu.op)
          ALU_OP_MOV: alu_out = alu.in0;
          ALU_OP_AND: alu_out = alu.in0 & alu.in1;
          ALU_OP_OR:  alu_out = alu.in0 | alu.in1;
          ALU_OP_XOR: alu_out = alu.in0 ^ alu.in1;
          ALU_OP_ADD: alu_out = alu.in0 + alu.in1;
          ALU_OP_SUB: alu_out = alu.in0 - alu.in1;
          ALU_OP_SHR: alu_out = {alu.in0[0], alu.in0[15:1]};
          ALU_OP_SHL: alu_out = {alu.in0[14:0], 1'b0};
        endcase
     end

   initial
     begin
        wram_bus.m_avalid <= 0;
        wram_bus.m_wvalid <= 0;
        ppu_bus.m_vld <= 0;
        bcd_in.vld <= 0;
        pc <= 16'h0200;
        state <= S_IFETCH;
     end

   chip8_cpu_bcd
     #(.WIDTH(CHIP8_DATA_WIDTH),
       .NUM_DIGITS(3))
   chip8_cpu_bcd_0
     (.i_clk(i_clk),
      .i_reset(i_reset),
      .i_data(bcd_in.data_in),
      .i_vld(bcd_in.vld),
      .o_data(bcd_out.data_out),
      .o_rdy(bcd_out.rdy));

   chip8_cpu_lfsr chip8_cpu_lfsr_0
     (.i_clk(i_clk),
      .i_reset(i_reset),
      .o_data(prng_out));

   function [15:0] hex_to_sprite_addr(input [3:0] hex);
      begin
         unique case (hex)
           4'h0: hex_to_sprite_addr = 16'h0030;
           4'h1: hex_to_sprite_addr = 16'h0035;
           4'h2: hex_to_sprite_addr = 16'h003A;
           4'h3: hex_to_sprite_addr = 16'h003F;
           4'h4: hex_to_sprite_addr = 16'h0044;
           4'h5: hex_to_sprite_addr = 16'h0049;
           4'h6: hex_to_sprite_addr = 16'h004E;
           4'h7: hex_to_sprite_addr = 16'h0053;
           4'h8: hex_to_sprite_addr = 16'h0058;
           4'h9: hex_to_sprite_addr = 16'h005D;
           4'hA: hex_to_sprite_addr = 16'h0062;
           4'hB: hex_to_sprite_addr = 16'h0067;
           4'hC: hex_to_sprite_addr = 16'h006C;
           4'hD: hex_to_sprite_addr = 16'h0071;
           4'hE: hex_to_sprite_addr = 16'h0076;
           4'hF: hex_to_sprite_addr = 16'h007B;
         endcase
      end
   endfunction
endmodule
