module chip8_cpu_bcd
  #(parameter WIDTH      = 8,
    parameter NUM_DIGITS = 3)
   (input logic                           i_clk,
    input logic                           i_reset,
    input logic [(WIDTH - 1):0]           i_data,
    input logic                           i_vld,
    output logic [(NUM_DIGITS * 4 - 1):0] o_data,
    output logic                          o_rdy);

   typedef enum {
      S_IDLE,
      S_SHIFT,
      S_CHECK_SHIFT_INDEX,
      S_ADD,
      S_CHECK_DIGIT_INDEX
   } state_t;

   logic [NUM_DIGITS*4-1:0] data_bcd;
   logic [WIDTH-1:0]        data_binary;
   logic [NUM_DIGITS-1:0]   digit_idx;
   logic [7:0]              it;
   logic [3:0]              bcd_digit;
   state_t                  state;

   always_ff @(posedge i_clk, posedge i_reset)
     begin: FSM
        if (i_reset) begin
           data_bcd <= 0;
           data_binary <= 0;
           digit_idx <= 0;
           it <= 0;
           o_rdy <= 1;
           state <= S_IDLE;
        end else begin
           case (state)
             S_IDLE:
               begin
                  if (i_vld == 1'b1) begin
                     data_binary <= i_data;
                     data_bcd <= 0;
                     o_rdy <= 0;
                     state <= S_SHIFT;
                  end else begin
                     o_rdy <= 1;
                     state <= S_IDLE;
                  end
               end
             S_SHIFT:
               begin
                  data_bcd <= data_bcd << 1;
                  data_bcd[0] <= data_binary[WIDTH - 1];
                  data_binary <= data_binary << 1;
                  state <= S_CHECK_SHIFT_INDEX;
               end
             S_CHECK_SHIFT_INDEX:
               begin
                  if (it == WIDTH - 1) begin
                     it <= 0;
                     o_rdy <= 1'b1;
                     state <= S_IDLE;
                  end else begin
                     it <= it + 1;
                     state <= S_ADD;
                  end
               end
             S_ADD:
               begin
                  if (bcd_digit > 4) begin
                     data_bcd[(digit_idx * 4) +: 4] <= bcd_digit + 3;
                  end
                  state <= S_CHECK_DIGIT_INDEX;
               end
             S_CHECK_DIGIT_INDEX:
               begin
                  if (digit_idx == NUM_DIGITS - 1) begin
                     digit_idx <= 0;
                     state <= S_SHIFT;
                  end else begin
                     digit_idx <= digit_idx + 1;
                     state <= S_ADD;
                  end
               end
             default: state <= S_IDLE;
           endcase
        end
     end

   assign bcd_digit = data_bcd[(digit_idx * 4) +: 4];
   assign o_data = data_bcd;
endmodule
