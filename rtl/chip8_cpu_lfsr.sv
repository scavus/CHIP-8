import defines::*;

module chip8_cpu_lfsr
  (input logic        i_clk,
   input logic        i_reset,
   output logic [7:0] o_data);

   logic [7:0] lfsr;
   logic       feedback;

   always_ff @(posedge i_clk, posedge i_reset)
     begin
        if(i_reset) begin
           lfsr <= '0;
        end else begin
           lfsr <= {lfsr[6:0], feedback};
        end
     end

   assign feedback = ~(lfsr[7] ^ lfsr[5] ^ lfsr[4] ^ lfsr[3]);
   assign o_data = lfsr;
endmodule
