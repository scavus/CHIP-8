module chip8_ppu
  (input logic                i_clk,
   intf_ram.master            vram_bus,
   intf_framebuffer_wt.master framebuffer_wt_bus,
   intf_ram.readonly          wram_bus,
   intf_ppu.slave             ppu_bus);

   typedef enum {
      S_IDLE,
      S_SETUP,
      S_EXECUTE,
      S_COPY,
      S_DONE
   } state_t;

   typedef enum {
      S_NOP,
      S_READ0,
      S_READ1,
      S_READ2,
      S_WRITE0,
      S_WRITE1,
      S_COPY_VRAM0,
      S_COPY_VRAM1,
      S_COPY_VRAM2
   } sub_state_t;

   logic [($clog2(CHIP8_VRAM_SIZE) - 1):0] vram_addr;
   logic [($clog2(CHIP8_VRAM_SIZE) - 1):0] vram_offset;
   logic [(CHIP8_ADDR_WIDTH - 1):0]        wram_offset;
   logic [15:0]                            blit_buffer;
   logic [15:0]                            sprite_buffer;
   logic [2:0]                             sprite_alignment;
   logic                                   sprite_collision;
   ppu_cmd_t                               cmd;
   state_t                                 state;
   sub_state_t                             sub_state;

   always_ff @(posedge i_clk)
     begin: FSM
        case (state)
          S_IDLE:
            begin
               ppu_bus.s_rdy <= 1;
               if (ppu_bus.m_vld) begin
                  cmd <= ppu_bus.cmd;
                  ppu_bus.s_rdy <= 0;
                  state <= S_SETUP;
               end
            end
          S_SETUP:
            begin
               case (cmd.op)
                 PPU_OP_CLEAR:       ppu_clear_setup();
                 PPU_OP_DRAW_SPRITE: ppu_draw_sprite_setup();
                 default:;
               endcase
            end
          S_EXECUTE:
            begin
               vram_bus.m_avalid <= 0;
               vram_bus.m_wvalid <= 0;
               wram_bus.m_avalid <= 0;
               ppu_bus.s_rdy <= 0;
               case (cmd.op)
                 PPU_OP_CLEAR:       ppu_clear_execute();
                 PPU_OP_DRAW_SPRITE: ppu_draw_sprite_execute();
                 default:;
               endcase
            end
          S_COPY: ppu_copy();
          S_DONE:
            begin
               vram_bus.m_avalid <= 0;
               vram_bus.m_wvalid <= 0;
               framebuffer_wt_bus.en <= 0;
               ppu_bus.collision <= sprite_collision;
               ppu_bus.s_rdy <= 1;
               sprite_collision <= 0;
               state <= S_IDLE;
            end
          default:;
        endcase
     end

   task ppu_clear_setup;
      ppu_bus.s_rdy <= 0;
      if (vram_bus.s_aready) begin
         vram_bus.m_aaddr <= 0;
         vram_bus.m_alen <= 255;
         vram_bus.m_awr <= 1;
         vram_bus.m_avalid <= 1;

         vram_offset <= 0;
         state <= S_EXECUTE;
      end
   endtask

   task ppu_clear_execute;
      if (vram_bus.s_wready) begin
         vram_bus.m_wdata <= 0;
         vram_bus.m_wvalid <= 1;
         if (vram_offset == 255) begin
            vram_offset <= 0;
            state <= S_COPY;
            sub_state <= S_COPY_VRAM0;
         end else begin
            vram_offset <= vram_offset + 1;
         end
      end
   endtask

   task ppu_draw_sprite_setup;
      ppu_bus.s_rdy <= 0;
      sprite_alignment <= cmd.x % 8;
      sprite_collision <= 0;
      vram_addr <= cmd.y * (64 >> 3) + (cmd.x >> 3);
      vram_offset <= 0;
      wram_offset <= 0;
      state <= S_EXECUTE;
      sub_state <= S_READ0;
   endtask

   task ppu_draw_sprite_execute;
      case (sub_state)
        S_READ0:
          begin
             if (vram_bus.s_aready && wram_bus.s_aready) begin
                vram_bus.m_aaddr <= vram_addr + vram_offset;
                vram_bus.m_alen <= 0;
                vram_bus.m_awr <= 0;
                vram_bus.m_avalid <= 1;
                vram_bus.m_rready <= 1;

                wram_bus.m_aaddr <= cmd.addr + wram_offset;
                wram_bus.m_alen <= 0;
                wram_bus.m_awr <= 0;
                wram_bus.m_avalid <= 1;
                wram_bus.m_rready <= 1;

                sub_state <= S_READ1;
             end
          end
        S_READ1:
          begin
             if (wram_bus.s_rvalid) begin
                sprite_buffer <= {wram_bus.s_rdata, 8'h00} >> sprite_alignment;
             end

             if (vram_bus.s_rvalid) begin
                blit_buffer[15:8] <= vram_bus.s_rdata;
             end

             if (vram_bus.s_aready && wram_bus.s_aready) begin
                if (sprite_alignment != 0) begin
                   vram_bus.m_aaddr <= vram_addr + vram_offset + 1;
                   vram_bus.m_alen <= 0;
                   vram_bus.m_awr <= 0;
                   vram_bus.m_avalid <= 1;
                   vram_bus.m_rready <= 1;
                end
                sub_state <= S_READ2;
             end
          end
        S_READ2:
          begin
             if (sprite_alignment != 0 && vram_bus.s_rvalid) begin
                blit_buffer[7:0] <= vram_bus.s_rdata;
             end

             if (vram_bus.s_aready) begin
                vram_bus.m_aaddr <= vram_addr + vram_offset;
                vram_bus.m_alen <= 0;
                vram_bus.m_awr <= 1;
                vram_bus.m_avalid <= 1;
                sub_state <= S_WRITE0;
             end
          end
        S_WRITE0:
          begin
             if (vram_bus.s_wready) begin
                vram_bus.m_wdata <= blit_buffer[15:8] ^ sprite_buffer[15:8];
                vram_bus.m_wvalid <= 1;
                sprite_collision <= sprite_collision | |(blit_buffer[15:8] & sprite_buffer[15:8]);
             end

             if (vram_bus.s_aready) begin
                if (sprite_alignment != 0) begin
                   vram_bus.m_aaddr <= vram_addr + vram_offset + 1;
                   vram_bus.m_alen <= 0;
                   vram_bus.m_awr <= 1;
                   vram_bus.m_avalid <= 1;
                end
                sub_state <= S_WRITE1;
             end
          end
        S_WRITE1:
          begin
             if (sprite_alignment != 0 && vram_bus.s_wready) begin
                vram_bus.m_wdata <= blit_buffer[7:0] ^ sprite_buffer[7:0];
                vram_bus.m_wvalid <= 1;
                sprite_collision <= sprite_collision | |(blit_buffer[7:0] & sprite_buffer[7:0]);
             end

             if (vram_bus.s_aready) begin
                if (wram_offset == cmd.h - 1) begin
                   state <= S_COPY;
                   sub_state <= S_COPY_VRAM0;
                end else begin
                   wram_offset <= wram_offset + 1;
                   vram_offset <= vram_offset + 8;
                   sub_state <= S_READ0;
                end
             end
          end
        default:;
      endcase
   endtask

   task ppu_copy;
      vram_bus.m_avalid <= 0;
      vram_bus.m_wvalid <= 0;
      framebuffer_wt_bus.en <= 0;
      case (sub_state)
        S_COPY_VRAM0:
          begin
             vram_offset <= 0;
             sub_state <= S_COPY_VRAM1;
          end
        S_COPY_VRAM1:
          begin
             if (vram_bus.s_aready) begin
                vram_bus.m_aaddr <= vram_offset;
                vram_bus.m_alen <= 0;
                vram_bus.m_awr <= 0;
                vram_bus.m_avalid <= 1;
                vram_bus.m_rready <= 1;
                sub_state <= S_COPY_VRAM2;
             end
          end
        S_COPY_VRAM2:
          begin
             if (vram_bus.s_rvalid) begin
                framebuffer_wt_bus.addr <= 8'(vram_offset);
                framebuffer_wt_bus.data <= vram_bus.s_rdata;
                framebuffer_wt_bus.en <= 1;
                if (vram_offset == 255) begin
                   state <= S_DONE;
                   sub_state <= S_NOP;
                end else begin
                   vram_offset <= vram_offset + 1;
                   sub_state <= S_COPY_VRAM1;
                end
             end
          end
      endcase
   endtask

endmodule
