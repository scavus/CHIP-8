`include "defines.sv"
import defines::*;

module chip8_ram
  #(parameter MEM_TYPE = "WRAM",
    parameter MEM_SIZE = CHIP8_WRAM_SIZE)
  (input logic    i_clk,
   intf_ram.slave bus
   // ,output logic [(CHIP8_DATA_WIDTH - 1):0] dbg_data [MEM_SIZE]
   );

   localparam MEM_ADDR_WIDTH = $clog2(MEM_SIZE);

   typedef enum {
      S_IDLE,
      S_READ,
      S_WRITE,
      S_DONE
   } state_t;

   logic [(CHIP8_DATA_WIDTH - 1):0] data [MEM_SIZE];
   logic [(MEM_ADDR_WIDTH - 1):0]   addr;
   logic [7:0]                      burst_length;
   logic [(MEM_ADDR_WIDTH - 1):0]   burst_offset, burst_offset_nxt;
   state_t                          state, state_nxt;

   always_ff @(posedge i_clk)
     begin
        burst_offset <= burst_offset_nxt;
        state <= state_nxt;

        case (state)
          S_IDLE:
            begin
               if (bus.m_avalid) begin
                  addr <= MEM_ADDR_WIDTH'(bus.m_aaddr);
                  burst_length <= bus.m_alen;
               end
               bus.s_wready <= 0;
               bus.s_rvalid <= 0;
               if (state_nxt == S_WRITE) begin
                  bus.s_wready <= 1;
               end
            end
          S_READ:
            begin
               if (bus.m_rready) begin
                  bus.s_rdata <= data[addr + burst_offset];
                  bus.s_rvalid <= 1;
               end
            end
          S_WRITE:
            begin
               if (bus.m_wvalid) begin
                  data[addr + burst_offset] <= bus.m_wdata;
               end
            end
          S_DONE:
            begin
               bus.s_wready <= 0;
               bus.s_rvalid <= 0;
            end
          default:;
        endcase
     end

   always_comb
     begin
        // FIXME:
        burst_offset_nxt = 0;
        state_nxt = state;

        if ((state == S_READ && bus.m_rready) || (state == S_WRITE && bus.m_wvalid)) begin
           if (burst_offset == MEM_ADDR_WIDTH'(burst_length)) begin
              burst_offset_nxt = 0;
              state_nxt = S_DONE;
           end else begin
              burst_offset_nxt = burst_offset + 1;
           end
        end

        case (state)
          S_IDLE:
            begin
               if (bus.m_avalid) begin
                  if (bus.m_awr) begin
                     state_nxt = S_WRITE;
                  end else begin
                     state_nxt = S_READ;
                  end
               end
            end
          S_DONE:
            begin
               state_nxt = S_IDLE;
            end
          default:;
        endcase
     end

   assign bus.s_aready = state_nxt == S_IDLE;

   // assign dbg_data = data;

   generate
      if (MEM_TYPE == "WRAM") begin
         initial
           begin
              $readmemh("font.hex", data, 12'h030, 12'h07F);
              $readmemh("test.hex", data, 12'h200, 12'hFFF);
           end
      end
   endgenerate
endmodule
