`include "defines.sv"
import defines::*;

module chip8_top
  (input logic       i_clk50,
   input logic [7:0] i_keypad4x4,
   output logic      o_vga_hsync,
   output logic      o_vga_vsync,
   output logic      o_vga_r,
   output logic      o_vga_g,
   output logic      o_vga_b);

   logic [4:0] pll_0_clks;
   logic       pll_0_locked;
   logic       clk_sys;
   logic       reset_sys;

   logic [4:0] pll_1_clks;
   logic       pll_1_locked;
   logic       clk_pix;
   logic       reset_pix;

   intf_ppu            ppu_bus ();
   intf_ram            vram_bus ();
   intf_framebuffer_wt framebuffer_wt_bus ();
   intf_framebuffer_rd framebuffer_rd_bus ();
   intf_ram            wram_bus_m [2] ();
   intf_ram            wram_bus_s ();

   always_comb
     begin: WRAM_INTERCONNECT
        wram_bus_s.m_aaddr = 0;
        wram_bus_s.m_alen = 0;
        wram_bus_s.m_awr = 0;
        wram_bus_s.m_avalid = 0;
        wram_bus_s.m_rready = 0;
        wram_bus_s.m_wvalid = 0;
        wram_bus_s.m_wdata = 0;

        wram_bus_m[0].s_aready = 0;
        wram_bus_m[0].s_rvalid = 0;
        wram_bus_m[0].s_rdata = 0;
        wram_bus_m[0].s_wready = 0;

        wram_bus_m[1].s_aready = 0;
        wram_bus_m[1].s_rvalid = 0;
        wram_bus_m[1].s_rdata = 0;
        wram_bus_m[1].s_wready = 0;

        if (ppu_bus.s_rdy) begin
           wram_bus_s.m_aaddr = wram_bus_m[0].m_aaddr;
           wram_bus_s.m_alen = wram_bus_m[0].m_alen;
           wram_bus_s.m_awr = wram_bus_m[0].m_awr;
           wram_bus_s.m_avalid = wram_bus_m[0].m_avalid;
           wram_bus_m[0].s_aready = wram_bus_s.s_aready;

           wram_bus_s.m_rready = wram_bus_m[0].m_rready;
           wram_bus_m[0].s_rvalid = wram_bus_s.s_rvalid;
           wram_bus_m[0].s_rdata = wram_bus_s.s_rdata;

           wram_bus_m[0].s_wready = wram_bus_s.s_wready;
           wram_bus_s.m_wvalid = wram_bus_m[0].m_wvalid;
           wram_bus_s.m_wdata = wram_bus_m[0].m_wdata;
        end else begin
           wram_bus_s.m_aaddr = wram_bus_m[1].m_aaddr;
           wram_bus_s.m_alen = wram_bus_m[1].m_alen;
           wram_bus_s.m_awr = wram_bus_m[1].m_awr;
           wram_bus_s.m_avalid = wram_bus_m[1].m_avalid;
           wram_bus_m[1].s_aready = wram_bus_s.s_aready;

           wram_bus_s.m_rready = wram_bus_m[1].m_rready;
           wram_bus_m[1].s_rvalid = wram_bus_s.s_rvalid;
           wram_bus_m[1].s_rdata = wram_bus_s.s_rdata;

           wram_bus_m[1].s_wready = wram_bus_s.s_wready;
           wram_bus_s.m_wvalid = wram_bus_m[1].m_wvalid;
           wram_bus_s.m_wdata = wram_bus_m[1].m_wdata;
        end
     end

   assign clk_sys = pll_0_clks[0];
   assign reset_sys = !pll_0_locked;

   assign clk_pix = pll_1_clks[0];
   assign reset_pix = !pll_1_locked;

   chip8_cpu cpu
     (.i_clk(clk_sys),
      .i_reset(reset_sys),
      .wram_bus(wram_bus_m[0]),
      .ppu_bus(ppu_bus));

   chip8_ppu ppu
     (.i_clk(clk_sys),
      .vram_bus(vram_bus),
      .framebuffer_wt_bus(framebuffer_wt_bus),
      .wram_bus(wram_bus_m[1].readonly),
      .ppu_bus(ppu_bus));

   chip8_ram
     #(.MEM_TYPE("WRAM"),
       .MEM_SIZE(CHIP8_WRAM_SIZE))
   wram
     (.i_clk(clk_sys),
      .bus(wram_bus_s));

   chip8_ram
     #(.MEM_TYPE("VRAM"),
       .MEM_SIZE(CHIP8_VRAM_SIZE))
   vram
     (.i_clk(clk_sys),
      .bus(vram_bus));

   chip8_video
     #(.SCALE(2))
   video
     (.i_clk(clk_pix),
      .i_reset(reset_pix),
      .o_vga_hsync(o_vga_hsync),
      .o_vga_vsync(o_vga_vsync),
      .o_vga_r(o_vga_r),
      .o_vga_g(o_vga_g),
      .o_vga_b(o_vga_b),
      .framebuffer_rd_bus(framebuffer_rd_bus));

   util_async_ram framebuffer_0
     (.i_wt_clk(clk_sys),
      .i_wt_en(framebuffer_wt_bus.en),
      .i_wt_addr(framebuffer_wt_bus.addr),
      .i_wt_data(framebuffer_wt_bus.data),
      .i_rd_clk(clk_pix),
      .i_rd_addr(framebuffer_rd_bus.addr),
      .o_rd_data(framebuffer_rd_bus.data));

`ifdef FPGA_INTEL
   ALTPLL pll_0
     (.inclk({1'h0, i_clk50}),
      .clk(pll_0_clks),
      .locked(pll_0_locked),
      .activeclock(),
      .areset(1'b0),
      .clkbad(),
      .clkena({6{1'b1}}),
      .clkloss(),
      .clkswitch(1'b0),
      .configupdate(1'b0),
      .enable0(),
      .enable1(),
      .extclk(),
      .extclkena({4{1'b1}}),
      .fbin(1'b1),
      .fbmimicbidir(),
      .fbout(),
      .fref(),
      .icdrclk(),
      .pfdena(1'b1),
      .phasecounterselect({4{1'b1}}),
      .phasedone(),
      .phasestep(1'b1),
      .phaseupdown(1'b1),
      .pllena(1'b1),
      .scanaclr(1'b0),
      .scanclk(1'b0),
      .scanclkena(1'b1),
      .scandata(1'b0),
      .scandataout(),
      .scandone(),
      .scanread(1'b0),
      .scanwrite(1'b0),
      .sclkout0(),
      .sclkout1(),
      .vcooverrange(),
      .vcounderrange());
   defparam
     pll_0.bandwidth_type = "AUTO",
     pll_0.clk0_divide_by = 1,
     pll_0.clk0_duty_cycle = 50,
     pll_0.clk0_multiply_by = 2,
     pll_0.clk0_phase_shift = "0",
     pll_0.compensate_clock = "CLK0",
     pll_0.inclk0_input_frequency = 20000,
     pll_0.intended_device_family = "Cyclone IV E",
     pll_0.lpm_type = "altpll",
     pll_0.operation_mode = "NORMAL",
     pll_0.pll_type = "AUTO",
     pll_0.self_reset_on_loss_lock = "OFF",
     pll_0.width_clock = 5;

   ALTPLL pll_1
     (.inclk({1'h0, i_clk50}),
      .clk(pll_1_clks),
      .locked(pll_1_locked),
      .activeclock(),
      .areset(1'b0),
      .clkbad(),
      .clkena({6{1'b1}}),
      .clkloss(),
      .clkswitch(1'b0),
      .configupdate(1'b0),
      .enable0(),
      .enable1(),
      .extclk(),
      .extclkena({4{1'b1}}),
      .fbin(1'b1),
      .fbmimicbidir(),
      .fbout(),
      .fref(),
      .icdrclk(),
      .pfdena(1'b1),
      .phasecounterselect({4{1'b1}}),
      .phasedone(),
      .phasestep(1'b1),
      .phaseupdown(1'b1),
      .pllena(1'b1),
      .scanaclr(1'b0),
      .scanclk(1'b0),
      .scanclkena(1'b1),
      .scandata(1'b0),
      .scandataout(),
      .scandone(),
      .scanread(1'b0),
      .scanwrite(1'b0),
      .sclkout0(),
      .sclkout1(),
      .vcooverrange(),
      .vcounderrange());
   defparam
     pll_1.bandwidth_type = "AUTO",
     pll_1.clk0_divide_by = 125,
     pll_1.clk0_duty_cycle = 50,
     pll_1.clk0_multiply_by = 63,
     pll_1.clk0_phase_shift = "0",
     pll_1.compensate_clock = "CLK0",
     pll_1.inclk0_input_frequency = 20000,
     pll_1.intended_device_family = "Cyclone IV E",
     pll_1.lpm_type = "altpll",
     pll_1.operation_mode = "NORMAL",
     pll_1.pll_type = "AUTO",
     pll_1.self_reset_on_loss_lock = "OFF",
     pll_1.width_clock = 5;
`endif

endmodule
