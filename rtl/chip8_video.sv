module chip8_video
  #(parameter SCALE = 0)
  (input logic         i_clk,
   input logic         i_reset,
   output logic        o_vga_hsync,
   output logic        o_vga_vsync,
   output logic        o_vga_r,
   output logic        o_vga_g,
   output logic        o_vga_b,
   intf_framebuffer_rd framebuffer_rd_bus);

   logic [9:0] sx, sy;
   logic       de;
   logic       hsync;
   logic       vsync;

   logic [9:0] vga_x_0, vga_y_0;
   logic       vga_de_0;
   logic       vga_hsync_0;
   logic       vga_vsync_0;

   logic [9:0] vga_x_1, vga_y_1;
   logic       vga_de_1;
   logic       vga_hsync_1;
   logic       vga_vsync_1;

   logic [9:0] vga_x_2, vga_y_2;
   logic       vga_de_2;
   logic       vga_hsync_2;
   logic       vga_vsync_2;

   always_ff @(posedge i_clk)
     begin
        framebuffer_rd_bus.addr <= (sy >> SCALE) * (64 >> 3) + ((sx >> SCALE) >> 3);
     end

   always_ff @(posedge i_clk)
     begin
        vga_x_0 <= sx >> SCALE;
        vga_y_0 <= sy >> SCALE;
        vga_de_0 <= de;
        vga_hsync_0 <= hsync;
        vga_vsync_0 <= vsync;
     end

   always_ff @(posedge i_clk)
     begin
        vga_x_1 <= vga_x_0;
        vga_y_1 <= vga_y_0;
        vga_de_1 <= vga_de_0;
        vga_hsync_1 <= vga_hsync_0;
        vga_vsync_1 <= vga_vsync_0;
     end

   always_ff @(posedge i_clk)
     begin
        vga_x_2 <= vga_x_1;
        vga_y_2 <= vga_y_1;
        vga_de_2 <= vga_de_1;
        vga_hsync_2 <= vga_hsync_1;
        vga_vsync_2 <= vga_vsync_1;
     end

   always_comb
     begin
        o_vga_hsync = vga_hsync_2;
        o_vga_vsync = vga_vsync_2;
        if (vga_de_2 && (vga_x_2 >= 0 && vga_x_2 < 64) && (vga_y_2 >= 0 && vga_y_2 < 32)) begin
           o_vga_r = framebuffer_rd_bus.data[7 - (vga_x_2 % 8)];
           o_vga_g = framebuffer_rd_bus.data[7 - (vga_x_2 % 8)];
           o_vga_b = framebuffer_rd_bus.data[7 - (vga_x_2 % 8)];
        end else begin
           o_vga_r = 1'b0;
           o_vga_g = 1'b0;
           o_vga_b = 1'b0;
        end
     end

   util_video_sync video_sync_0
     (.i_clk(i_clk),
      .i_reset(i_reset),
      .o_sx(sx),
      .o_sy(sy),
      .o_hsync(hsync),
      .o_vsync(vsync),
      .o_de(de));

endmodule
