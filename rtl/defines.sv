`ifndef DEFINES_SV
`define DEFINES_SV

package defines;
   parameter CHIP8_NUM_REGISTERS  = 16;
   parameter CHIP8_INSTR_WIDTH    = 16;
   parameter CHIP8_DATA_WIDTH     = 8;
   parameter CHIP8_ADDR_WIDTH     = 16;
   parameter CHIP8_DATA_BUS_WIDTH = 8;
   parameter CHIP8_ADDR_BUS_WIDTH = 12;
   parameter CHIP8_STACK_SIZE     = 32;
   parameter CHIP8_WRAM_SIZE      = 2**CHIP8_ADDR_BUS_WIDTH;
   parameter CHIP8_VRAM_SIZE      = (64 * 32) / CHIP8_DATA_WIDTH;

   typedef enum {
      PPU_OP_NOP,
      PPU_OP_CLEAR,
      PPU_OP_DRAW_SPRITE
   } ppu_op_t;

   typedef struct packed {
      ppu_op_t                         op;
      logic [(CHIP8_DATA_WIDTH - 1):0] x;
      logic [(CHIP8_DATA_WIDTH - 1):0] y;
      logic [3:0]                      h;
      logic [(CHIP8_ADDR_WIDTH - 1):0] addr;
   } ppu_cmd_t;
endpackage

interface intf_ppu ();
   defines::ppu_cmd_t cmd;
   logic              collision;
   logic              m_vld;
   logic              s_rdy;

   modport master(input collision, s_rdy,
                  output cmd, m_vld);
   modport slave(input cmd, m_vld,
                 output collision, s_rdy);
endinterface

interface intf_ram ();
   logic [(CHIP8_ADDR_WIDTH - 1):0] m_aaddr;
   logic [7:0]                      m_alen;
   logic                            m_awr;
   logic                            m_avalid;
   logic                            s_aready;

   logic                            m_rready;
   logic                            s_rvalid;
   logic [(CHIP8_DATA_WIDTH - 1):0] s_rdata;

   logic                            s_wready;
   logic                            m_wvalid;
   logic [(CHIP8_DATA_WIDTH - 1):0] m_wdata;

   modport master(input s_aready, s_rvalid, s_wready, s_rdata,
                  output m_aaddr, m_alen, m_awr, m_avalid, m_rready, m_wvalid, m_wdata);
   modport readonly(input s_aready, s_rvalid, s_rdata,
                    output m_aaddr, m_alen, m_awr, m_avalid, m_rready);
   modport slave(input m_aaddr, m_alen, m_awr, m_avalid, m_rready, m_wvalid, m_wdata,
                 output s_aready, s_rvalid, s_wready, s_rdata);

   task request_read
     (input logic [(CHIP8_ADDR_WIDTH - 1):0] rd_addr,
      input logic [7:0]                      rd_length);
      m_aaddr <= rd_addr;
      m_alen <= rd_length;
      m_awr <= 0;
      m_avalid <= 1;
   endtask

   task request_write
     (input logic [(CHIP8_ADDR_WIDTH - 1):0] wt_addr,
      input logic [7:0]                      wt_length);
      m_aaddr <= wt_addr;
      m_alen <= wt_length;
      m_awr <= 1;
      m_avalid <= 1;
   endtask
endinterface

interface intf_framebuffer_wt ();
   logic [7:0] addr;
   logic [7:0] data;
   logic       en;

   modport master(output addr, data, en);
   modport slave(input addr, data, en);
endinterface

interface intf_framebuffer_rd ();
   logic [7:0] addr;
   logic [7:0] data;

   modport master(input data, output addr);
   modport slave(input addr, output data);
endinterface

`endif
