module util_async_ram
  (input logic        i_wt_clk,
   input logic        i_wt_en,
   input logic [7:0]  i_wt_addr,
   input logic [7:0]  i_wt_data,
   input logic        i_rd_clk,
   input logic [7:0]  i_rd_addr,
   output logic [7:0] o_rd_data);

`ifdef FPGA_INTEL
   ALTSYNCRAM altsyncram
     (.address_a(i_wt_addr),
      .address_b(i_rd_addr),
      .clock0(i_wt_clk),
      .clock1(i_rd_clk),
      .data_a(i_wt_data),
      .wren_a(i_wt_en),
      .q_b(o_rd_data),
      .aclr0(1'b0),
      .aclr1(1'b0),
      .addressstall_a(1'b0),
      .addressstall_b(1'b0),
      .byteena_a(1'b1),
      .byteena_b(1'b1),
      .clocken0(1'b1),
      .clocken1(1'b1),
      .clocken2(1'b1),
      .clocken3(1'b1),
      .data_b({8{1'b1}}),
      .eccstatus(),
      .q_a(),
      .rden_a(1'b1),
      .rden_b(1'b1),
      .wren_b(1'b0));
   defparam
     altsyncram.address_aclr_b = "NONE",
     altsyncram.address_reg_b = "CLOCK1",
     altsyncram.clock_enable_input_a = "BYPASS",
     altsyncram.clock_enable_input_b = "BYPASS",
     altsyncram.clock_enable_output_b = "BYPASS",
     altsyncram.intended_device_family = "Cyclone IV E",
     altsyncram.lpm_type = "altsyncram",
     altsyncram.numwords_a = 256,
     altsyncram.numwords_b = 256,
     altsyncram.operation_mode = "DUAL_PORT",
     altsyncram.outdata_aclr_b = "NONE",
     altsyncram.outdata_reg_b = "CLOCK1",
     altsyncram.power_up_uninitialized = "FALSE",
     altsyncram.widthad_a = 8,
     altsyncram.widthad_b = 8,
     altsyncram.width_a = 8,
     altsyncram.width_b = 8,
     altsyncram.width_byteena_a = 1;
`endif

endmodule
