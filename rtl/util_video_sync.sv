module util_video_sync
  (input  logic       i_clk,
   input  logic       i_reset,
   output logic [9:0] o_sx,
   output logic [9:0] o_sy,
   output logic       o_hsync,
   output logic       o_vsync,
   output logic       o_de);

   // Horizontal timings
   localparam HA_END = 640 - 1,     // End of active pixels
              HS_STA = HA_END + 16, // Sync starts after front porch
              HS_END = HS_STA + 96, // Sync ends
              LINE   = 800 - 1;     // Last pixel on line (after back porch)

   // Vertical timings
   localparam VA_END = 480 - 1,     // End of active pixels
              VS_STA = VA_END + 10, // Sync starts after front porch
              VS_END = VS_STA + 2,  // Sync ends
              SCREEN = 525 - 1;     // Last line on screen (after back porch)

   always_ff @(posedge i_clk, posedge i_reset)
     begin: SCREEN_COORDS
        if (i_reset) begin
           o_sx <= 0;
           o_sy <= 0;
        end else begin
           if (o_sx == LINE) begin
              o_sx <= 0;
              o_sy <= (o_sy == SCREEN) ? 0 : o_sy + 1;
           end else begin
              o_sx <= o_sx + 1;
           end
        end
     end

   always_comb
     begin
        o_hsync = ~(o_sx >= HS_STA && o_sx < HS_END);
        o_vsync = ~(o_sy >= VS_STA && o_sy < VS_END);
        o_de = (o_sx <= HA_END && o_sy <= VA_END);
     end
endmodule
